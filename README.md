# README #

### What is this repository for? ###

* MiniVegas is a Unity project of games not really tied to Las Vegas at all.
* Currently these games are:
    1.  GoFish: A FPS networked GoFish Game (Currently in development)
    1.  ShootingGallery: An FPS leap motion game where making a gun gesture shoots out balls you use to hit flying targets. (Basic prototype done, but not actively in development)

### How do I get set up? ###

* install Unity > 5 (Currently 5.2)
* `git clone <this_repo>`
* For the shooting gallery game, you will need the `Oculus SDK` and `LeapMotion SDK` installed. You may also needs these to develop in the project.

### Who do I talk to? ###

* Repo owner and sole developer (adioevan@gmail.com)