﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class MenuButton : MonoBehaviour {
    enum ButtonType { Quit, Begin, Settings, GoFishBtn, ShootingGalleryBtn };

    [SerializeField]
    private ButtonType buttonType;

    [SerializeField]
    private uint defaultRoomSize = 6;

    [SerializeField]
    private float requestTimeout = 10f;

    private Button button;
    private NetworkManager networkManager;

    private GameObject messageGO;
    private Text messageText;

    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => ExecuteButtonFunction());

        networkManager = NetworkManager.singleton;

        messageGO = GameObject.FindGameObjectWithTag("MessageText");
    }

    /// <summary>
    /// Allows button listener to run function easily by button type.
    /// </summary>
    public void ExecuteButtonFunction()
    {
        switch (buttonType)
        {
            case ButtonType.Quit:
                QuitGame();
                break;
            case ButtonType.Settings:
                StartCoroutine(ShowMessageForSeconds("Settings menu coming soon...", 3));
                break;
            case ButtonType.Begin:
                LoadLobbyScene();
                break;
            case ButtonType.GoFishBtn:
                PlayGoFish();
                break;
            case ButtonType.ShootingGalleryBtn:
                PlayShootingGallery();
                break;
            default:
                Debug.LogError(string.Format("No function for button type {0}", buttonType.ToString()));
                StartCoroutine(ShowMessageForSeconds("Unknown error", 3));
                break;
        }
    }

    private void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #elif UNITY_WEBPLAYER
        Application.OpenURL(webplayerQuitURL);
        #else
        Application.Quit();
        #endif
    }

    private IEnumerator ShowMessageForSeconds(string message, float seconds)
    {
        if (messageGO == null)
        {
            Debug.LogError("Please add a message element to show a mwessage");
        }
        else
        {
            messageText = messageGO.GetComponent<Text>();
            messageText.text = message;
            yield return new WaitForSeconds(seconds);
            messageText.text = "";
        }
    }

    private void LoadLobbyScene()
    {
        SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
    }

    private void PlayGoFish()
    {
        if (networkManager.matchMaker == null)
        {
            networkManager.StartMatchMaker();
        }
        // Get Match list
        networkManager.matchMaker.ListMatches(0, 10, "", true, 0, 0, OnMatchList);
    }

    private void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            if (matches.Count == 0)
            {
                // Make match
                networkManager.matchMaker.CreateMatch("default", defaultRoomSize,
                                                  true, "", "", "",
                                                  0, 0,
                                                  networkManager.OnMatchCreate);
                return;
            }
            foreach (var matchInfo in matches)
            {
                if (matchInfo.currentSize != matchInfo.maxSize)
                {
                    JoinMatch(matchInfo);
                    StartCoroutine(WaitForJoin());
                    return;
                }
            }
        }
        else
        {
            Debug.LogError("Couldn't connect to match maker");
        }
    }

    public void JoinMatch(MatchInfoSnapshot match)
    {
        networkManager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, networkManager.OnMatchJoined);
        MatchInfo _matchInfo = networkManager.matchInfo;
        if (_matchInfo != null)
        {
            networkManager.matchMaker.DropConnection(_matchInfo.networkId,
                                         _matchInfo.nodeId,
                                         _matchInfo.domain,
                                         networkManager.OnDropConnection);
            networkManager.StopHost();
        }
    }

    IEnumerator WaitForJoin()
    {
        if (messageGO == null)
        {
            Debug.LogError("Please add a message element to show a mwessage");
        }
        else
        {
            messageText = messageGO.GetComponent<Text>();
            float countDown = requestTimeout;
            while (countDown > 0)
            {
                messageText.text = "Joining...(" + countDown + ")";
                yield return new WaitForSeconds(1);
                countDown--;
            }

            // Failed to connect
            messageText.text = "Failed to connect.";
            yield return new WaitForSeconds(1);

            MatchInfo _matchInfo = networkManager.matchInfo;
            if (_matchInfo != null)
            {
                networkManager.matchMaker.DropConnection(_matchInfo.networkId,
                                             _matchInfo.nodeId,
                                             _matchInfo.domain,
                                             networkManager.OnDropConnection);
                networkManager.StopHost();
            }
        }
    }

    private void PlayShootingGallery()
    {
        // This is an offline game type so just load the scene
        SceneManager.LoadScene("ShootingGallery", LoadSceneMode.Single);
    }
}
