﻿using UnityEngine;
using UnityEngine.VR;

public class DisableVR : MonoBehaviour {
	void Start ()
	{
        	VRSettings.enabled = false;
	}
}
