﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

using Leap;
using Leap.Unity;

public class ShootingGalleryPlayer : MonoBehaviour
{
    private LeapProvider _leapProvider;
    private bool _justShot;
    private Text _scoreUIText;
    private float _timeSinceLastShot;
    public float GunCooldown = 1;
    public int Score = 0;

    [SerializeField]
    private LayerMask mask;

    [SerializeField]
    private GameObject hitEffectPrefab;

    [SerializeField]
    private GameObject bulletTracePrefab;

    [SerializeField]
    private float range = 100f;


    // Use this for initialization
    void Start()
    {
        _scoreUIText = GetComponentInChildren<Text>();
        _leapProvider = GetComponentInChildren <LeapProvider>();
    }
	
	// Update is called once per frame
    void Update()
    {
        _scoreUIText.text = "Score: " + Score;
        var frame = _leapProvider.CurrentFrame;
        foreach (Hand hand in frame.Hands)
        {
            if (hand.Confidence < 0.5f) return;
            // Could use detectors and logic gates but doing this in code is easier for me
            if (FireGestureDetected(hand) && !_justShot)
            {
                _timeSinceLastShot = 0.0f;
                Fire(hand);
            }
        }
        _timeSinceLastShot += Time.deltaTime;
        if (_timeSinceLastShot > GunCooldown)
        {
            _justShot = false;
        }
        
    }

    private bool FireGestureDetected(Hand hand)
    {
        var index = GetFingerByType(hand, Finger.FingerType.TYPE_INDEX);
        var nonActionFingersClasped = from finger in hand.Fingers
            where finger.Type != Finger.FingerType.TYPE_INDEX && finger.Type != Finger.FingerType.TYPE_THUMB
                  && !finger.IsExtended select finger;
        return (index.IsExtended && nonActionFingersClasped.Count() == 3);
    }

    private Finger GetFingerByType(Hand hand, Finger.FingerType type)
    {
        return hand.Fingers[(int)type];
    }

    void Fire(Hand hand)
    {
        var index = GetFingerByType(hand, Finger.FingerType.TYPE_INDEX);
        CreateEffect(index.TipPosition.ToVector3(), index.Direction.ToVector3(), bulletTracePrefab);
        RaycastHit _hit;
        if (Physics.Raycast(index.TipPosition.ToVector3(),
                            index.Direction.ToVector3(),
                            out _hit,
                            range,
                            mask))
        {
            if (_hit.collider.gameObject.layer == LayerMask.NameToLayer("ClayPigeon"))
            {
                Score++;
                Destroy(_hit.collider.gameObject);
            }
            CreateEffect(_hit.point, _hit.normal, hitEffectPrefab);
        }
        _justShot = true;
    }

    void CreateEffect(Vector3 pos, Vector3 normal, GameObject effect)
    {
        GameObject _go = Instantiate(effect, pos, Quaternion.LookRotation(normal));
        Destroy(_go, 0.1f);
    }
}
