﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class ClayPigeonLauncher : MonoBehaviour
{
    public GameObject[] ClayPigeonGameObjects;
    private Queue<GameObject> _spawnedClayPigeons= new Queue<GameObject>();
    private ShootingGalleryPlayer _player;
    public float LaunchForce = 1000.0f;
    public float RotationForce = 10.0f;
    public int ClayPigeonLifetime = 3;
    private int _spawnRate;

    // Use this for initialization
	void Start ()
	{
	    StartCoroutine(LaunchClayPigeon());
	    _player = GameObject.FindGameObjectWithTag("Player").GetComponent<ShootingGalleryPlayer>();
	    // UnityEngine.VR.VRSettings.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _spawnRate = 3 - _player.Score/5;
	    while (_spawnedClayPigeons.Count > 0)
            Destroy(_spawnedClayPigeons.Dequeue(), ClayPigeonLifetime);
	}

    private IEnumerator LaunchClayPigeon()
    {
        while (true)
        {
            if (ClayPigeonGameObjects.Length == 0)
            {
                Debug.LogError("Clay Pigeon collection is empty. Please fill collection with game objects in editor.");
            }
            var spawnPos = new Vector3(transform.position.x, transform.position.y + 0.25f, GetComponent<Collider>().bounds.size.z * Random.Range(-0.5f, 0.5f));
            var clayPigeonIndex = Random.Range(0, ClayPigeonGameObjects.Length);
            GameObject go = Instantiate(ClayPigeonGameObjects[clayPigeonIndex], spawnPos, transform.rotation);
            // Label instantiate prefab as ClayPigeon for collision detection
            go.layer = LayerMask.NameToLayer("ClayPigeon");
            // the prefab does not have a rigidbody so add one
            var rb =  go.AddComponent<Rigidbody>();
            rb.AddForceAtPosition(Vector3.up * RotationForce, Vector3.right);
            rb.AddForce(new Vector3(0, LaunchForce, 0));
            // Make the claypigeons hang in the air a bit
            rb.drag = Random.Range(0, 4);
            _spawnedClayPigeons.Enqueue(go);
            yield return new WaitForSeconds(_spawnRate);
        }
        
    }
}
