﻿using System;
using UnityEngine;

public class Weapon : MonoBehaviour, ICardContainer
{
    public string Name = "DefaultWeaponName";
    public float Damage = 0.1f;
    public float Range = 100f;
    public float FireRate = 0f;
    public bool Primary = false;
    private Card card;
    [SerializeField]
    private Renderer m_renderer;
    private Renderer[] renderers;

    public void Start()
    {
        renderers = GetComponentsInChildren<MeshRenderer>();
    }

    private void Update()
    {
        Name = gameObject.name;
        foreach (var renderer in renderers)
        {
            renderer.enabled = Primary;
        }
        UpdateGraphic();
    }

    public Card GetCard()
    {
        return card;
    }

    public void SetCard(Card inCard)
    {
        card = inCard;
    }

    public void UpdateGraphic()
    {
        if (card.Equals(null))
        {
            return;
        }
        m_renderer.material.mainTexture = Resources.Load<Texture>(card.GetTexturePath());
    }
}