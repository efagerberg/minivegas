﻿using UnityEngine;
using UnityEngine.Networking;

public class ObjectPool : MonoBehaviour, IObjectPool
{
    [SerializeField]
    private int m_ObjectPoolSize;
    public GameObject m_Prefab;
    public GameObject[] m_Pool;

    //private NetworkHash128 assetID { get; set; }

    void Start()
    {
        //assetID = m_Prefab.GetComponent<NetworkIdentity>().assetId;
        m_Pool = new GameObject[m_ObjectPoolSize];
        for (int i = 0; i < m_ObjectPoolSize; ++i)
        {
            m_Pool[i] = Instantiate(m_Prefab, Vector3.zero, Quaternion.identity);
            m_Pool[i].SetActive(false);
            m_Pool[i].transform.SetParent(gameObject.transform);
        }

        //ClientScene.RegisterSpawnHandler(assetID, SpawnObject, UnSpawnObject);
    }

    public GameObject GetFromPool(Vector3 position)
    {
        foreach (var obj in m_Pool)
        {
            if (!obj.activeInHierarchy)
            {
                obj.transform.position = position;
                obj.SetActive(true);
                return obj;
            }
        }
        Debug.LogError("Could not grab object from pool, nothing available");
        return null;
    }

    public GameObject GetFromPool()
    {
        foreach (var obj in m_Pool)
        {
            if (!obj.activeInHierarchy)
            {
                obj.SetActive(true);
                return obj;
            }
        }
        Debug.LogError("Could not grab object from pool, nothing available");
        return null;
    }

    public GameObject SpawnObject(Vector3 position, NetworkHash128 assetId)
    {
        var obj = GetFromPool(position);
        return obj;
    }

    public void UnSpawnObject(GameObject spawned)
    {
        Debug.Log("Re-pooling object " + spawned.name);
        spawned.transform.SetParent(gameObject.transform);
        spawned.name = m_Prefab.name;
        spawned.layer = LayerMask.NameToLayer("Default");
        spawned.SetActive(false);
    }
}