﻿using UnityEngine;

public class PlayerUI : MonoBehaviour {
    [SerializeField]
    private GameObject pauseMenu;

    private UIBarFill[] fills;

    private IPlayerAttributeManager m_playerAttributeManager;
    public IPlayerAttributeManager PlayerAttributeManager {
        get
        {
            return m_playerAttributeManager;
        }
        set
        {
            value.OnDeath += Reset;
            m_playerAttributeManager = value;
            SetupFills();
        }
    }

    private void Start()
    {
        PauseMenu.IsOn = false;
        fills = GetComponentsInChildren<UIBarFill>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }
    }

    public void TogglePauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        PauseMenu.IsOn = pauseMenu.activeSelf;
    }

    public void SetupFills()
    {
        foreach (var fill in fills)
        {
            fill.PlayerAttributeManager = PlayerAttributeManager;
        }
    }

    public void Reset()
    {
        foreach (var fill in fills)
        {
            fill.Reset();
        }
    }
}
