﻿using System.Collections.Generic;
using UnityEngine;

public class LocalPlayerHandRepresenter : MonoBehaviour {

    [SerializeField]
    private List<UICard> uiCards = new List<UICard>();

    private GameObject handPanel;
    private ObjectPool cardUIPool;
    private SyncListCard cards;


    void Start () {
        cards = GetComponent<HandManager>().Cards;
        var playerUIGO = GameObject.FindGameObjectWithTag(Constants.PLAYER_UI_TAG);
        handPanel = playerUIGO.transform.FindChild("HandPanel").gameObject;
        var cardUIPoolGO = GameObject.FindGameObjectWithTag("CardUIPool");
        cardUIPool = cardUIPoolGO.GetComponent<ObjectPool>();
    }

	void Update () {
        if (uiCards.Count == 0 && cards.Count > 0)
            CreateUICardsFromHand();

        UpdateUICards();
    }

    void UpdateUICards() {
        for (int i = 0; i < cards.Count; i++)
        {
            if (!cards[i].Equals(uiCards[i].GetCard()))
            {
                uiCards[i].SetCard(cards[i]);
            }
        }
    }

    void CreateUICardsFromHand()
    {
        foreach (var card in uiCards)
        {
            cardUIPool.UnSpawnObject(card.gameObject);
        }
        uiCards.Clear();
        foreach (var card in cards)
        {
            var uiCardGO = cardUIPool.GetFromPool();
            var uiCardComponent = uiCardGO.GetComponent<UICard>();
            uiCardComponent.SetCard(card);
            uiCardGO.name = card.ToString();
            uiCards.Add(uiCardComponent);
            uiCardGO.transform.SetParent(handPanel.transform);
        }
    }
}
