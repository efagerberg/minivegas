﻿using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class SyncListCard : SyncListStruct<Card> { }

public class HandManager : NetworkBehaviour
{
    private SyncListCard m_cards = new SyncListCard();
    public Card SelectedCard
    {
        get
        {
            return m_cards.Where(card => card.Selected).Single();
        }
    }

    private Dealer dealer;
    public SyncListCard Cards
    {
        get
        {
            return m_cards;
        }
    }

    [SerializeField]
    private int HandSize = 7;

    void Start()
    {
        if (isLocalPlayer)
        {
            CmdGetDealer();
            CmdAskDealerForCards();
        }
    }

    private void Update()
    {
        if (isLocalPlayer)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                CmdSelectNextCard();
            }
        }
    }

    private void selectNextCard()
    {
        var selectedCardIndex = m_cards.ToList().IndexOf(SelectedCard);
        var nextCardIndex = selectedCardIndex < m_cards.Count - 1 ? selectedCardIndex + 1 : 0;
        // Unfortunately you can only sync when resetting the whole object;
        // card.Selected = true; won't work
        // Deselect last card
        var cardToDeselect = SelectedCard;
        cardToDeselect.Selected = false;
        m_cards[selectedCardIndex] = cardToDeselect;

        // Select next card
        var cardToSelect = m_cards[nextCardIndex];
        cardToSelect.Selected = true;
        m_cards[nextCardIndex] = cardToSelect;
        selectedCardIndex = nextCardIndex;
    }

    [Command]
    private void CmdSelectNextCard()
    {
        selectNextCard();
    }

    [Command]
    void CmdReDraw()
    {
        dealer.ReturnCards(m_cards);
        AskDealerForCards();
    }

    [Command]
    void CmdGetDealer()
    {
        var dealerGO = GameObject.Find("Dealer");
        dealer = dealerGO.GetComponent<Dealer>();
    }

    [Server]
    void AskDealerForCards()
    {
        dealer.DrawCards(HandSize, m_cards);
    }

    [Command]
    void CmdAskDealerForCards()
    {
        AskDealerForCards();
    }

    public bool ContainsValue(Card.Values value)
    {
        return m_cards.Any(c => c.Value == value);
    }

    public bool ContainsSuit(Card.Suits suit)
    {
        return m_cards.Any(c => c.Suit == suit);
    }

    public bool ContainsCard(Card card)
    {
        return m_cards.Any(c => c.Equals(card));
    }
}
