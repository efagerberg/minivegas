﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


class PlayerSetupManager : NetworkBehaviour, IPlayerSetupManager
{
    [SerializeField]
    private Behaviour[] disableBehavioursOnDeath;
    private bool[] wasEnabled;

    [SerializeField]
    private GameObject[] disableGameObjectsOnDeath;

    private bool isFirstSetup = true;

    [SerializeField]
    Behaviour[] localOnlyComponents;

    [SerializeField]
    Behaviour[] remoteOnlyComponents;

    [SerializeField]
    GameObject playerGraphics;

    private GameObject playerUIGO;
    private PlayerUI playerUI;

    private IPlayerAttributeManager playerAttributeManager;

    private

    void Start()
    {
        playerAttributeManager = GetComponent<IPlayerAttributeManager>();
        playerAttributeManager.OnDeath += DisablePlayer;
        playerAttributeManager.OnRespawn += BeginPlayerSetup;
        // Disable components that should only be
        // active on the player that we control
        if (!isLocalPlayer)
        {
            SetBehavioursEnabledStatus(localOnlyComponents, false);
            AssignRemoteLayer();
            AssignRemoteTag();
        }
        else
        {
            playerUIGO = GameObject.FindGameObjectWithTag(Constants.PLAYER_UI_TAG);
            playerUI = playerUIGO.GetComponent<PlayerUI>();
            playerUI.PlayerAttributeManager = GetComponent<IPlayerAttributeManager>();
            SetBehavioursEnabledStatus(remoteOnlyComponents, false);
            // Set Layer for playerGraphics
            Util.SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(Constants.DONT_DRAW_LAYER));
            GameManager.instance.SetSceneCameraActive(false);
            SetupPlayer();
        }
    }

    public void DisablePlayer()
    {
        var rb = GetComponent<Rigidbody>();
        rb.useGravity = false;

        // Disable components, collider and gameobjects
        for (int i = 0; i < disableBehavioursOnDeath.Length; i++)
        {
            disableBehavioursOnDeath[i].enabled = false;
        }

        for (int i = 0; i < disableGameObjectsOnDeath.Length; i++)
        {
            disableGameObjectsOnDeath[i].SetActive(false);
        }

        //Spawn DeathFX
        //var _gfx = Instantiate(deathEffect, transform.position, Quaternion.identity);
        //Destroy(_gfx, 3f);

        Collider _col = GetComponent<Collider>();
        if (_col != null)
            _col.enabled = false;

        if (isLocalPlayer)
        {
            GameManager.instance.SetSceneCameraActive(true);
            playerUIGO.SetActive(false);
        }
    }

    private void SetDefaults()
    {
        // Reset the components
        for (int i = 0; i < disableBehavioursOnDeath.Length; i++)
        {
            disableBehavioursOnDeath[i].enabled = wasEnabled[i];
        }

        // Enable the gameobjects
        for (int i = 0; i < disableGameObjectsOnDeath.Length; i++)
        {
            disableGameObjectsOnDeath[i].SetActive(true);
        }

        // Enable collider
        Collider _col = GetComponent<Collider>();
        if (_col != null)
            _col.enabled = true;


        var rb = GetComponent<Rigidbody>();
        rb.useGravity = true;
        // Create Spawn Effect
        //var _gfx = Instantiate(spawnEffect, transform.position, Quaternion.identity);
        //Destroy(_gfx, 3f);
    }

    public void BeginPlayerSetup()
    {
        if (isLocalPlayer)
        {
            GameManager.instance.SetSceneCameraActive(false);
            playerUIGO.SetActive(true);
        }
        CmdSetupPlayer();
    }

    private void CmdSetupPlayer()
    {
        RpcSetupPlayer();
    }

    private void RpcSetupPlayer()
    {
        SetupPlayer();
    }

    private void SetupPlayer()
    {
        if (isFirstSetup)
        {
            wasEnabled = new bool[disableBehavioursOnDeath.Length];
            for (int i = 0; i < wasEnabled.Length; i++)
            {
                wasEnabled[i] = disableBehavioursOnDeath[i].enabled;
            }
            isFirstSetup = false;
        }
        SetDefaults();
    }

    private void AssignRemoteLayer()
    {
        gameObject.layer = LayerMask.NameToLayer(Constants.REMOTE_PLAYER_LAYER);
    }

    private void AssignRemoteTag()
    {
        gameObject.tag = Constants.REMOTE_PLAYER_TAG;
    }

    private void SetBehavioursEnabledStatus(Behaviour[] collection, bool status)
    {
        for (int i = 0; i < collection.Length; i++)
        {
            collection[i].enabled = status;
        }
    }

    private void SetGameObjectActiveStates(GameObject[] collection, bool status)
    {
        for (int i = 0; i < collection.Length; i++)
        {
            collection[i].SetActive(status);
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        var _netID = GetComponent<NetworkIdentity>().netId;
        Player _player = GetComponent<Player>();

        GameManager.RegisterPlayer(_netID, _player);
    }

    // When we are destroyed
    private void OnDisable()
    {
        if (isLocalPlayer)
            GameManager.instance.SetSceneCameraActive(true);
        var playerNetID = GetComponent<NetworkIdentity>().netId;
        GameManager.UnregisterPlayer(playerNetID);
    }
}
