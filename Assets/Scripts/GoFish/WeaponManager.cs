﻿using UnityEngine;
using UnityEngine.Networking;

public class WeaponManager : NetworkBehaviour, IWeaponManager
{
    [SerializeField]
    private LayerMask mask;

    [SerializeField]
    private Camera cam;

    private Weapon[] weapons;

    private HandManager handManager;

    [SyncVar]
    private int currentWeaponIndex;

    private int nextWeaponIndex
    {
        get
        {
            return currentWeaponIndex < weapons.Length - 1 ? currentWeaponIndex + 1 : 0;
        }
    }

    private int lastWeaponIndex
    {
        get
        {
            return currentWeaponIndex > 0 ? currentWeaponIndex - 1: weapons.Length - 1;
        }
    }

    void Start()
    {
        handManager = GetComponent<HandManager>();
        weapons = GetComponentsInChildren<Weapon>();
        if (isLocalPlayer)
        {
            foreach (var weapon in weapons)
                Util.SetLayerRecursively(weapon.gameObject, LayerMask.NameToLayer(Constants.WEAPON_LAYER_NAME));

            CmdEquipFirstWeapon();
        }

        if (cam == null)
        {
            Debug.LogError("PlayerShoot: No camera referenced!");
            enabled = false;
        }
    }

    private void Update()
    {
        if (PauseMenu.IsOn)
        {
            return;
        }
        if (isLocalPlayer)
        {
            var currentWeapon = weapons[currentWeaponIndex];
            if (Input.GetButtonDown("Fire1"))
            {
                if (currentWeapon.FireRate <= 0)
                {
                    ShootWeapon();
                }
                else
                {
                    InvokeRepeating("ShootWeapon", 0, 1f / currentWeapon.FireRate);
                }
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                CancelInvoke("ShootWeapon");
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                CmdEquipNextWeapon();
            }
        }
        UpdateWeapon();
    }

    void UpdateWeapon()
    {
        weapons[lastWeaponIndex].Primary = false;
        weapons[currentWeaponIndex].Primary = true;
        if (handManager.Cards.Count != 0)
        {
            weapons[currentWeaponIndex].SetCard(handManager.SelectedCard);
        }
    }

    [Command]
    void CmdEquipFirstWeapon()
    {
        currentWeaponIndex = 0;
    }

    [Command]
    void CmdEquipNextWeapon()
    {
        EquipNextWeapon();
    }

    [Client]
    public void EquipNextWeapon()
    {
        currentWeaponIndex = nextWeaponIndex;
    }

    [Client]
    public void ShootWeapon()
    {
        // We are shooting, call the on shoot method on the server.
        // CmdOnShoot();

        RaycastHit _hit;
        var currentWeapon = weapons[currentWeaponIndex];
        if (Physics.Raycast(cam.transform.position,
                            cam.transform.forward,
                            out _hit,
                            currentWeapon.Range,
                            mask))
        {
            if (_hit.collider.tag == Constants.REMOTE_PLAYER_TAG)
            {
                var _playerNetId = _hit.collider.gameObject.GetComponent<NetworkIdentity>().netId;
                CmdPlayerShot(_playerNetId, currentWeapon.Damage);
            }
            // CmdOnHit(_hit.point, _hit.normal);
        }
    }

    [Command]
    void CmdPlayerShot(NetworkInstanceId _playerID, float _damage)
    {
        Debug.Log(_playerID + " has been shot");
        Player _player = GameManager.GetPlayer(_playerID);
        var _playerAttributeManager = _player.GetComponent<PlayerAttributeManager>();
        _playerAttributeManager.TakeDamage(_damage);
    }
}
