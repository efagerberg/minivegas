﻿using System.Collections.Generic;
using UnityEngine.Networking;

public class Dealer : NetworkBehaviour
{
    private Deck deck;

    void Start()
    {
        deck = new Deck().Shuffle();
    }

    public void DrawCards(int amount, IList<Card> cards)
    {
        for (var i = 0; i < amount; i++)
        {
            var card = deck.RemoveFromTop();
            if (i == 0)
            {
                card.Selected = true;
            }
            cards.Add(card);
        }
    }

    public void ReturnCards(IList<Card> cards)
    {
        deck.ReturnCards(cards);
    }
}
