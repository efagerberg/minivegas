﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

interface IPlayerSetupManager
{
    void BeginPlayerSetup();
    void DisablePlayer();
}
