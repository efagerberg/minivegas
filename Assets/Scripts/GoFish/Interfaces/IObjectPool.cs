﻿using UnityEngine;
using UnityEngine.Networking;

public interface IObjectPool
{
    GameObject GetFromPool(Vector3 position);
    GameObject SpawnObject(Vector3 position, NetworkHash128 assetId);
    void UnSpawnObject(GameObject spawned);
}