﻿using UnityEngine;

public delegate void OnDeathEvent();
public delegate void OnRespawnEvent();

public interface IPlayerAttributeManager
{
    float Health { get; }
    float ThrusterFuel { get; }
    float Stamina { get; }
    void CmdReset();
    void TakeDamage(float amount);
    void CmdTakeDamage(float amount);
    // Event Handler
    event OnDeathEvent OnDeath;
    event OnRespawnEvent OnRespawn;
}

