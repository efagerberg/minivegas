﻿public interface ICardContainer {
    Card GetCard();
    void SetCard(Card inCard);
    void UpdateGraphic();
}
