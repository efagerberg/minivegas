﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponManager
{
    void EquipNextWeapon();
    void ShootWeapon();
}
