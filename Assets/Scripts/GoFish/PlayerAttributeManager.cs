﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerAttributeManager : NetworkBehaviour, IPlayerAttributeManager
{
    [SyncVar]
    protected bool isDead = false;

    [SyncVar]
    private float health = 1f;
    [SerializeField]
    private float healthRegen = 0.01f;

    private float stamina = 1f;
    [SerializeField]
    private float staminaBurn = 1f;
    [SerializeField]
    private float staminaRegen = 0.1f;

    private float thrusterFuel = 1f;
    [SerializeField]
    protected float thrusterFuelBurn = 1f;
    [SerializeField]
    protected float thrusterFuelRegen = 0.3f;

    private PlayerUI playerUI;

    public event OnDeathEvent OnDeath;
    public event OnRespawnEvent OnRespawn;

    public float Stamina
    {
        get
        {
            return stamina;
        }
        private set
        {
            stamina = Mathf.Clamp(value, 0f, 1f);
        }
    }

    public float ThrusterFuel
    {
        get
        {
            return thrusterFuel;
        }

        private set
        {
            thrusterFuel = Mathf.Clamp(value, 0f, 1f);
        }
    }

    public float Health
    {
        get
        {
            return health;
        }
        private set
        {
            health = Mathf.Clamp(value, 0f, 1f);
        }
    }

    private void Start()
    {
        playerUI = GameObject.FindGameObjectWithTag(Constants.PLAYER_UI_TAG).GetComponent<PlayerUI>();
        if (isLocalPlayer) CmdReset();
    }

    private void Update()
    {
        if (isLocalPlayer)
        {
            if (ThrusterFuel < 1f) ThrusterFuel += thrusterFuelRegen * Time.deltaTime;
            if (Stamina < 1f) Stamina += staminaRegen * Time.deltaTime;
            if (Health < 1f) Health += healthRegen * Time.deltaTime;
            if (Input.GetButton("Jump")) ThrusterFuel -= thrusterFuelBurn * Time.deltaTime;
            if (Input.GetKey(KeyCode.LeftShift)) Stamina -= staminaBurn * Time.deltaTime;
        }
    }

    public void Die()
    {
        isDead = true;
        OnDeath();
        //Spawn DeathFX
        //var _gfx = Instantiate(deathEffect, transform.position, Quaternion.identity);
        //Destroy(_gfx, 3f);
        StartCoroutine(Respawn());
    }

    [ClientRpc]
    public void RpcDie()
    {
        Die();
    }

    [Command]
    public void CmdDie()
    {
        RpcDie();
    }

    public IEnumerator Respawn()
    {
        yield return new WaitForSeconds(GameManager.instance.matchSettings.respawnTime);
        Transform _spawnPoint = NetworkManager.singleton.GetStartPosition();
        transform.position = _spawnPoint.position;
        transform.rotation = _spawnPoint.rotation;
        // Buffer for particle spawning
        yield return new WaitForSeconds(0.1f);
        OnRespawn();

        if (isLocalPlayer)
        {
            CmdReset();
            stamina = 1f;
            thrusterFuel = 1f;
        }
        playerUI.Reset();
        Debug.Log(transform.name + " respawned.");
    }

    [Server]
    public void TakeDamage(float amount)
    {
        if (isDead) return;

        Health -= amount;

        Debug.Log(transform.name + " now has " + Health + " health.");

        if (health <= 0)
        {
            CmdDie();
        }
    }

    [Command]
    public void CmdTakeDamage(float amount) { TakeDamage(amount); }

    [Command]
    public void CmdReset()
    {
        Reset();
    }

    [Server]
    private void Reset()
    {
        isDead = false;
        health = 1f;
    }
}
