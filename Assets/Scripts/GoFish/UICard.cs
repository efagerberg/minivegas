﻿using UnityEngine;
using UnityEngine.UI;

public class UICard : MonoBehaviour, ICardContainer {

    [SerializeField]
    private Card card;
    [SerializeField]
    private Image outlineImage;
    [SerializeField]
    private Image cardImage;

    private void Update()
    {
        name = card.ToString();
        UpdateGraphic();
    }

    public Card GetCard()
    {
        return card;
    }

    public void SetCard(Card inCard)
    {
        card = inCard;
    }

    public void UpdateGraphic()
    {
        if (card.Equals(null))
        {
            return;
        }
        cardImage.sprite = Resources.Load<Sprite>(card.GetSpritePath());
        outlineImage.enabled = card.Selected;
    }
}
