﻿using UnityEngine;
using UnityEngine.UI;

public class UIBarFill : MonoBehaviour
{
    private enum BarType { Health, Stamina, Thruster }
    [SerializeField]
    private BarType type;

    private Image fill;
    private RectTransform rectTransform;
    private float originalAlpha;

    private float value;

    public IPlayerAttributeManager PlayerAttributeManager { get; set; }

    void Start()
    {
        fill = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
        originalAlpha = fill.color.a;

        // Start off transparent
        fill.CrossFadeAlpha(0f, 0f, false);
    }

    void Update()
    {
        if (PlayerAttributeManager == null) return;
        float value = getValue();
        rectTransform.localScale = new Vector3(value, 1f, 1f);
        handleAutoHide(value);
    }

    float getValue()
    {
        switch (type)
        {
            case BarType.Health:
                return PlayerAttributeManager.Health;
            case BarType.Stamina:
                return PlayerAttributeManager.Stamina;
            case BarType.Thruster:
                return PlayerAttributeManager.ThrusterFuel;
            default:
                Debug.LogError("No scaling value found for bar type " + type);
                return 0f;
        }
    }

    void handleAutoHide(float value)
    {
        if (value == 1f && fill.color.a != 0f)
        {
            fill.CrossFadeAlpha(0f, 0.5f, false);
        }
        else
        {
            fill.CrossFadeAlpha(originalAlpha, 0.5f, false);
        }
    }

    public void Reset()
    {
        fill.CrossFadeAlpha(0f, 0f, false);
    }
}
