public static class Constants {
    public const float DEVIATION_UI_CONVERSION_FACTOR = 5.0f;
    public const float MIN_DEVIATION = 3.5f;
    public const float MAX_WEAPON_DEVIATION = 10.0f;
    public const float MAX_BASE_DEVIATION = 20.0f;
    public const float DEVIATION_DECAY_FACTOR = 0.2f;
    public const float DEVIATION_DAMPENING_CONSTANT = 0.25f;

    public const int DEFAULT_DECK_SIZE = 52;

    public const string REMOTE_PLAYER_LAYER = "RemotePlayer";
    public const string REMOTE_PLAYER_TAG = "RemotePlayer";

    public const string LOCAL_PLAYER_LAYER = "LocalPlayer";
    public const string LOCAL_PLAYER_TAG = "LocalPlayer";
    public const string HAND_LAYER = "CardHand";

    public const string DONT_DRAW_LAYER = "DontDraw";

    public const string PLAYER_UI_TAG = "PlayerUI";
    public const string WEAPON_LAYER_NAME = "Weapon";
}
