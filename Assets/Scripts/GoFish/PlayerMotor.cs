﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{

    [SerializeField]
    private Camera cam;

    private Vector3 m_velocity = Vector3.zero;
    public Vector3 Velocity { get { return m_velocity; } }
    private Vector3 m_rotation = Vector3.zero;
    private float m_cameraRotationX = 0f;
    private float m_currentCameraRotationX = 0f;
    private Vector3 m_thrusterForce = Vector3.zero;
    public Vector3 ThrusterForce { get { return m_thrusterForce; } }

    [SerializeField]
    private float cameraRotationLimit = 85f;

    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move(Vector3 _velocity)
    {
        m_velocity = _velocity;
    }

    public void Rotate(Vector3 _rotation)
    {
        m_rotation = _rotation;
    }

    public void RotateCamera(float _cameraRotation)
    {
        m_cameraRotationX = _cameraRotation;
    }

    public void ApplyThruster(Vector3 _thrusterForce)
    {
        m_thrusterForce = _thrusterForce;
    }

    void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    }

    void PerformMovement()
    {
        if (m_velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + m_velocity * Time.fixedDeltaTime);
        }

        if (m_thrusterForce != Vector3.zero)
        {
            rb.AddForce(m_thrusterForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }

    void PerformRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(m_rotation));
        if (cam != null)
        {
            m_currentCameraRotationX -= m_cameraRotationX;
            m_currentCameraRotationX = Mathf.Clamp(m_currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);
            cam.transform.localEulerAngles = new Vector3(m_currentCameraRotationX, 0, 0);
        }
    }
}