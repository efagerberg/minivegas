﻿using UnityEngine;

[ExecuteInEditMode]
public class ChipStack : MonoBehaviour {

    [SerializeField]
    private int chipAmount;

    [SerializeField]
    private GameObject chipPrefab;

    [SerializeField]
    private Vector3 chipScale = Vector3.one;

	void Start () {
        var startPosition = gameObject.transform.position;
        for (int i = 0; i < chipAmount; i++)
        {
            var yOffset = chipPrefab.GetComponent<Renderer>().bounds.size.y * chipScale.y;
            var posY = i * yOffset;
            var pos = new Vector3(startPosition.x, posY, startPosition.z);
            var go = Instantiate(chipPrefab, pos, Quaternion.identity);
            go.transform.localScale = chipScale;
            go.transform.SetParent(transform);

        }
	}
}
