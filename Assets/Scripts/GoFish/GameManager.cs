﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public MatchSettings matchSettings;

    [SerializeField]
    private GameObject sceneCamera;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one GameManager found");
        }
        else
        {
            instance = this;
        }
    }

    public void SetSceneCameraActive(bool _isActive)
    {
        if (sceneCamera == null)
        {
            Debug.LogWarning("SceneCamera not found on game manager");
            return;
        }
        sceneCamera.SetActive(_isActive);
    }

    #region Player tracking
    private const string PLAYER_ID_PREFIX = "Player ";
    private static Dictionary<NetworkInstanceId, Player> players = new Dictionary<NetworkInstanceId, Player>();

    public static void RegisterPlayer(NetworkInstanceId netID, Player player)
    {
        int playerCount = players.Count + 1;
        string _playerName = PLAYER_ID_PREFIX + playerCount;
        players.Add(netID, player);
        player.transform.name = _playerName;
    }

    public static void UnregisterPlayer(NetworkInstanceId playerID)
    {
        players.Remove(playerID);
    }

    public static Player GetPlayer(NetworkInstanceId playerID)
    {
        return players[playerID];
    }

    public static int GetPlayerCount() { return players.Count; }

    //void OnGUI()
    //{
    //    GUILayout.BeginArea(new Rect(200, 200, 200, 500));
    //    GUILayout.BeginVertical();

    //    foreach (string _playerID in players.Keys)
    //    {
    //        GUILayout.Label(_playerID + " - " + players[_playerID].transform.name);
    //    }

    //    GUILayout.EndVertical();
    //    GUILayout.EndArea();
    //}
    #endregion
}