﻿using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    private IPlayerAttributeManager playerAttributeManager;

    private void Start()
    {
        playerAttributeManager = GetComponent<IPlayerAttributeManager>();
    }

    private void Update()
    {
        if (!isLocalPlayer)
            return;

        if (Input.GetKeyDown(KeyCode.K))
        {
            CmdTest();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit _hit;
            if (Physics.Raycast(transform.position, transform.forward,
                        out _hit, 1f))
            {
                Debug.Log("Hit");
            }
        }
    }

    [Command]
    private void CmdTest()
    {
        playerAttributeManager.TakeDamage(0.25f);
    } 
}