﻿using System.Collections.Generic;
using UnityEngine;

public class RemotePlayerHandRepresenter : MonoBehaviour
{
    [SerializeField]
    private Vector3 handGOLocalPosition;
    [SerializeField]
    private float cardSpacing;

    private ObjectPool cardPool;
    private GameObject handGO;

    [SerializeField]
    private List<CardComponent> cardComponents = new List<CardComponent>();
    private SyncListCard cards;

    [SerializeField]
    private bool debug;

    void CreateHandContainerGO()
    {
        handGO = new GameObject("CardHandGO");
        handGO.transform.SetParent(gameObject.transform.FindChild("Graphics"));
        handGO.transform.localRotation = Quaternion.identity;
        handGO.transform.localPosition = handGOLocalPosition;
    }

    void Start()
    {
        CreateHandContainerGO();
        cards = GetComponent<HandManager>().Cards;
        var cardPoolGO = GameObject.FindGameObjectWithTag("CardPool");
        cardPool = cardPoolGO.GetComponent<ObjectPool>();
    }

    void Update()
    {
        if (cardComponents.Count == 0 && cards.Count > 0 && cardPool != null)
            CreateCardComponentsFromHand();
        if ((handGO.transform.childCount <= 0 && cardComponents.Count > 0) || debug)
            UpdateCardPositions();
        UpdateCardComponents();
    }

    void UpdateCardComponents()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (!cards[i].Equals(cardComponents[i].GetCard()))
            {
                cardComponents[i].SetCard(cards[i]);
            }
        }
    }

    void CreateCardComponentsFromHand()
    {
        foreach (var cardComponent in cardComponents)
        {
            cardPool.UnSpawnObject(cardComponent.gameObject);
        }
        cardComponents.Clear();
        foreach (var card in cards)
        {
            var cardGO = cardPool.GetFromPool();
            var cardComponent = cardGO.GetComponent<CardComponent>();
            cardComponent.SetCard(card);
            cardComponents.Add(cardComponent);
        }
    }

    void UpdateCardPositions()
    {
        handGO.transform.localPosition = handGOLocalPosition;
        var numCards = cardComponents.Count;
        for (int cardIndex = 0; cardIndex < numCards; cardIndex++)
        {
            var cardComponent = cardComponents[cardIndex];
            cardComponent.transform.parent = handGO.transform;
            cardComponent.transform.localRotation = Quaternion.identity;
            var localPositionXOffset = (cardSpacing * cardIndex) - (numCards * (cardSpacing) / 2.0f);
            var localPositionOffset = new Vector3(localPositionXOffset, 0, 0);
            cardComponent.transform.localPosition = handGO.transform.localPosition +
                                                    localPositionOffset;
        }
    }
}
