using UnityEngine;

[RequireComponent(typeof(PlayerMotor))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float walkSpeed = 5f;
    [SerializeField]
    private float runSpeed = 7f;
    [SerializeField]
    private float lookSensitivity = 3f;

    [SerializeField]
    private float thrusterForce = 1500f;

    private IPlayerAttributeManager playerAtrributeManager;
    private PlayerMotor motor;

    // Use this for initialization
    void Start()
    {
        motor = GetComponent<PlayerMotor>();
        playerAtrributeManager = GetComponent<IPlayerAttributeManager>();
    }

    void Update()
    {
        if (PauseMenu.IsOn)
        {
            HandlePause();
            return;
        }

        HandleCursorLock();
        HandleMovement();
        HandleRotation();
        HandleThrusters();
    }

    void HandlePause()
    {
        if (Cursor.lockState != CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        motor.Move(Vector3.zero);
        motor.Rotate(Vector3.zero);
        motor.RotateCamera(0f);
    }

    void HandleCursorLock()
    {
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    void HandleMovement()
    {
        float _xMov = Input.GetAxis("Horizontal");
        float _zMov = Input.GetAxis("Vertical");

        Vector3 _movHorizontal = transform.right * _xMov;
        Vector3 _movVerical = transform.forward * _zMov;

        var speed = Input.GetKey(KeyCode.LeftShift) && playerAtrributeManager.Stamina >= 0.01f ? runSpeed : walkSpeed;
        Vector3 _velocity = (_movHorizontal + _movVerical).normalized * speed;

        motor.Move(_velocity);
    }

    void HandleRotation()
    {
        float _yRot = Input.GetAxisRaw("Mouse X");
        Vector3 _rotation = new Vector3(0f, _yRot, 0f * lookSensitivity);

        motor.Rotate(_rotation);

        float _xRot = Input.GetAxisRaw("Mouse Y");
        float _cameraRotationX = _xRot * lookSensitivity;
        motor.RotateCamera(_cameraRotationX);
    }

    void HandleThrusters()
    {
        Vector3 _thursterForce = Vector3.zero;

        if (Input.GetButton("Jump") && playerAtrributeManager.ThrusterFuel > 0f)
        {
            if (playerAtrributeManager.ThrusterFuel >= 0.01f)
                _thursterForce = Vector3.up * thrusterForce;
        }
        motor.ApplyThruster(_thursterForce);
    }
}