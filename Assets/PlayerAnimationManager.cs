﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationManager : MonoBehaviour {

    [SerializeField]
    private Animator animator;
    [SerializeField]
    private PlayerMotor motor;

	
	// Update is called once per frame
	void Update () {
        animator.SetFloat("ForwardVelocity", motor.Velocity.z);
        animator.SetFloat("Speed", Mathf.Abs(motor.Velocity.magnitude));
    }
}
